﻿using System;
using Xamarin.Forms;

namespace LiveCustomer.Chat
{
    class MyDataTemplateSelector : Xamarin.Forms.DataTemplateSelector
    {
        public MyDataTemplateSelector()
        {
            // Retain instances!
            this.incomingDataTemplate = new DataTemplate(typeof(ChatLeftMessageItemTemplate));
            this.outgoingDataTemplate = new DataTemplate(typeof(ChatRightMessageItemTemplate));
        }

        protected override DataTemplate OnSelectTemplate(object item, BindableObject container)
        {
            //return this.incomingDataTemplate;
            var CurrentUser = item as SignalRClient.EventMessage;
            if (CurrentUser.User == null)
                return this.incomingDataTemplate;
            return !CurrentUser.User.IsAdmin ? this.incomingDataTemplate : this.outgoingDataTemplate;
        }
        private readonly DataTemplate incomingDataTemplate;
        private readonly DataTemplate outgoingDataTemplate;
    }
}

﻿using System;
namespace LiveCustomer.Chat
{
    public class Global
    {
        public string SignalRHubBaseUri()
        {
            //return "http://3027596f.ngrok.io";
            return "https://admin.livecustomer.chat";
        }
        public string SignalRHubFullUrl()
        {
            return SignalRHubBaseUri() + "/signalr/hubs";
        }
    }
    public class GenericResult
    {
        public bool Success { get; set; }
        public string Message { get; set; }
    }
    public interface IDeviceScreenSize
    {
        ScreenSize GetScreenSize();        
    }
    public class ScreenSize
    {
        public int X { get; set; }
        public int Y { get; set; }
    }
}
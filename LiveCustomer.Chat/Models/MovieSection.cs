using System.Collections.Generic;

namespace LiveCustomer.Chat
{
	public class MovieSection
	{
		public string Title { get; set; }

		public List<Movie> Content { get; set; }
	}
}

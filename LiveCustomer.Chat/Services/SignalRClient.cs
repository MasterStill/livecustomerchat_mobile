﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Runtime.CompilerServices;
using System.Threading.Tasks;
using Microsoft.AspNet.SignalR.Client;
using Prism.Mvvm;
using System.Diagnostics;
using System.IO;
using Java.IO;
using System.Threading;
using System.Text;
using Xamarin.Forms;

namespace LiveCustomer.Chat
{
    public class SignalRClient : INotifyPropertyChanged
    {
        private HubConnection Connection;
        private IHubProxy ChatHubProxy;
        //private List<CustomerSupportUser> user;
        public delegate void JoiningAllClients(List<CustomerSupportUser> u);
        public event JoiningAllClients onJoiningAllClients;

        public delegate void JoiningSingleClient(CustomerSupportUser u);
        public event JoiningSingleClient onJoiningSingleClient;


        public delegate void LeftSingleClient(CustomerSupportUser u);
        public event LeftSingleClient onUserOffline;


        public delegate void CustomerInfoChanged(CustomerInfo c);
        public event CustomerInfoChanged onCustomerInfoChanged;


        public delegate void GetClientGeoInformation(GeoinfoAndPages c);
        public event GetClientGeoInformation onGeoInformation;


        public delegate void NewMessage(MessageViewModel message);
        public event NewMessage onNewMessage;


        public delegate void SendPreviousMessage(GroupEventMessage message);
        public event SendPreviousMessage onSendPreviousMessage;


        public delegate void Callingsupport(string ConnectionId);
        public event Callingsupport onCallingForSupport;

        public delegate void AdminAuthorization1(List<EventMessage> GroupMessage);
        public event AdminAuthorization1 onAdminAuthorization;
                

        public delegate void LoginAttempt(GenericResult result);
        public event LoginAttempt onLoginAttempt;
        //  private INavigationService NavigationService;
        private class DebugTextWriter : TextWriter
        {
            private StringBuilder buffer;

            public DebugTextWriter()
            {
                buffer = new StringBuilder();
            }

            public override void Write(char value)
            {
                switch (value)
                {
                    case '\n':
                        return;
                    case '\r':
                        Debug.WriteLine(buffer.ToString());
                        buffer.Clear();
                        return;
                    default:
                        //Debug.WriteLine(buffer.ToString());
                        //buffer.Clear();
                        buffer.Append(value);
                        break;
                }
            }

            public override void Write(string value)
            {
                Debug.WriteLine(value);

            }
            #region implemented abstract members of TextWriter
            public override Encoding Encoding
            {
                get { throw new NotImplementedException(); }
            }
            #endregion
        }
        public SignalRClient(string url)//, INavigationService navigationService=null)
        {            
            // this.NavigationService = navigationService;
            var querystringData = new Dictionary<string, string>();
            querystringData.Add("isAdmin", "true");
            querystringData.Add("isMobileDevice", "true");
            Connection = new HubConnection(url, querystringData);            
            var writer = new DebugTextWriter();
            Connection.TraceWriter = writer;
            Connection.TraceLevel = TraceLevels.All;
            Connection.StateChanged += (StateChange obj) =>
            {
                OnPropertyChanged("ConnectionState");
            };

            ChatHubProxy = Connection.CreateHubProxy("CustomerSupportHub");

            ChatHubProxy.On<List<CustomerSupportUser>>("onJoiningAllClients", (user) =>
            {
                onJoiningAllClients?.Invoke(user);
            });

            //ChatHubProxy.On<List<GroupEventMessage>>("onJoiningAllSupportMessages", (user) =>
            //{
            //    onJoiningAllSupportMessages?.Invoke(user);
            //});

            ChatHubProxy.On<MessageViewModel>("onNewMessage", (message) =>
            {
                onNewMessage?.Invoke(message);
            });

            ChatHubProxy.On<CustomerSupportUser>("onNewUserAuthorization", (message) =>
            {
                onJoiningSingleClient?.Invoke(message);
            });

            ChatHubProxy.On<CustomerSupportUser>("onUserOffline", (message) =>
            {
                onUserOffline?.Invoke(message);
            });

            ChatHubProxy.On<GenericResult>("onLoginAttempt", (message) =>
            {
                //if (message.Success)
                //{
                //    NavigationService.NavigateAsync("NavigationPage/ChatHeadPage");
                //}
                onLoginAttempt?.Invoke(message);
            });

            ChatHubProxy.On<string>("onCallingForSupport", (connectionId) =>
            {
                onCallingForSupport?.Invoke(connectionId);
            });
            ChatHubProxy.On<CustomerInfo>("onCustomerInfoChanged", (message) =>
            {
                onCustomerInfoChanged?.Invoke(message);
            });


            ChatHubProxy.On<GeoinfoAndPages>("onGeoInformation", (message) =>
            {
                onGeoInformation?.Invoke(message);
            });
          
            ChatHubProxy.On<List<EventMessage>>("onAdminAuthorization", (message) =>
            {
                onAdminAuthorization?.Invoke(message);
            });


            ChatHubProxy.On<GroupEventMessage>("onSendPreviousMessage", (message) =>
            {
                onSendPreviousMessage?.Invoke(message);
            });
        }

        public void SendMessage(string username, string text)
        {
            ChatHubProxy.Invoke("Message", username, text);
        }
        public void UserTyping(string GroupName)
        {
            ChatHubProxy.Invoke("UserTyping", GroupName);           
        }
        public void UserStoppedTyping(string GroupName)
        {
            ChatHubProxy.Invoke("UserStoppedTyping", GroupName);           
        }
        public void AdminAuthorization(string username, string password)
        {
            ChatHubProxy.Invoke("AdminAuthorization", username, password);
        }
        public void FetchMePreviousMessage(string groupNAme, int pageNo=0)
        {
            ChatHubProxy.Invoke("SendPreviousMessage", groupNAme, pageNo);
        }
        public void SupportRequestAccepted(string ConnectionId)
        {
            ChatHubProxy.Invoke("SupportRequestAccepted", ConnectionId);
        }
        public void ClientGeoInformation(string groupName)
        {
            ChatHubProxy.Invoke("GeoInformation", groupName);
        }
        public Task Start()
        {
            return Connection.Start();
        }

        public bool IsConnectedOrConnecting
        {
            get
            {
                return Connection.State != ConnectionState.Disconnected;
            }
        }

        public ConnectionState ConnectionState { get { return Connection.State; } }

        public static async Task<SignalRClient> CreateAndStart(string url)
        {
            var client = new SignalRClient(url);
            await client.Start();
            return client;
        }

        public event PropertyChangedEventHandler PropertyChanged;

        protected virtual void OnPropertyChanged([CallerMemberName] string propertyName = null)
        {
            var handler = PropertyChanged;
            if (handler != null)
                handler(this, new PropertyChangedEventArgs(propertyName));
        }
        public class CustomerSupportUser:BindableBase 
        {
            //public string connectionId { get; set; }
            public Guid id { get; set; }
            public string fullName { get; set; }
            public string CurrentHost { get; set; }
            public string status { get; set; }
            public string email { get; set; }
            public List<string> ConnectionId { get; set; }
            public string memberType { get; set; }
            public List<string> Sites { get; set; }
            public string avatar { get; set; }
            public string contextName { get; set; }
            public string deviceId { get; set; }
            public string CurrentUrl { get; set; }
            public string userName { get; set; }
            public string phoneNumber { get; set; }
            public string groupName { get; set; }
            public bool online { get; set; }
            public bool Busy { get; set; }
            public int DbAdminId { get; set; }
            public int TotalTabs { get; set; }
            public string webSite { get; set; }
            public List<string> Groups { get; set; }
            public bool AwaitingSupport { get; set; }
            public string CurrentPage { get; set; }
            public string LastMessage { get; set; }
            public List<SitePage> SitePages { get; set; }
            public DateTime LastActive { get; set; }
            public bool IsAdmin { get; set; }
            public GeoInformation GeoInformation { get; set; }
            public IpInfoDb IpInfoDb { get; set; }
            public DateTime DisconnectedTime { get; set; }

        }
        public class IpInfoDb
        {
            public IpInfoDb(string json)
            {

            }
            public string ipaddress { get; set; }
            public string countrycode { get; set; }
            public string countryname { get; set; }
            public string cityname { get; set; }
            public string regionname { get; set; }
            public string zipcode { get; set; }
            public string timezone { get; set; }
            public string latitude { get; set; }
            public string longitude { get; set; }
        }
        public class SitePage
        {
            public string Url { get; set; }
            public string PageTitle { get; set; }
            public DateTime DateTime { get; set; }
        }
        //public class SignalRMessageReceive
        //{
        //    public string Message { get; set; }
        //    public CustomerSupportUser User { get; set; }

        //}
        public class GeoInformation
        {
            public GeoInformation(string json)
            {

            }
            public string ip { get; set; }
            public string country_code { get; set; }
            public string country_name { get; set; }
            public string region_code { get; set; }
            public string region_name { get; set; }
            public string city { get; set; }
            public string zip_code { get; set; }
            public string time_zone { get; set; }
            public string latitude { get; set; }
            public string longitude { get; set; }
            public string metro_code { get; set; }
        }
        public class GroupEventMessage
        {
            public GroupEventMessage()
            {
                this.EventMessage = new List<EventMessage>();
            }
            public string groupName { get; set; }
            public List<EventMessage> EventMessage { get; set; }
        }
        public class EventMessage
        {
            public EventMessage()
            {
                this.User = new CustomerSupportUserViewModel(new CustomerSupportUser());
            }

            public DateTime DateTime { get; set; }
            public enumTicketMesssages Type { get; set; }
            public CustomerSupportUserViewModel User { get; set; }            
            public string Message { get; set; }
        }
        public class CustomerSupportUserViewModel
        {
            public CustomerSupportUserViewModel(CustomerSupportUser c)
            {
                fullName = c.fullName;
                avatar = c.avatar;
                IsAdmin = c.IsAdmin;
            }
            public string fullName { get; set; }
            public string avatar { get; set; }
            public bool IsAdmin { get; set; }
        }      
        public enum enumTicketMesssages
        {
            //[Description(" Started Typing")]
            Typing = 1,
            //[Description(" StoppedTyping Typing")]
            StoppedTyping = 2,

            //[Description(" has joined the Support")]
            JoinedSupport = 3,

            //[Description(" has left the Support")]
            LeftSupport = 3,

            //[Description("Text Message")]
            TextMessage = 3,

            //[Description("Image Message")]
            ImageMessage = 3,
            //[Description("Push Marco")]
            PushMarco = 3,

        }
        public class CustomerInfo
        { 
            public string fullName { get; set; }
            public string groupName { get; set; }
            public string email { get; set; }
            public string phoneNumber { get; set; }
            public string avatar { get; set; }
        }
        public class MessageViewModel
        {
            public string groupName { get; set; }
            public string Message { get; set; }
            public CustomerSupportUser User { get; set; }
        }
        public class GeoinfoAndPages
        {
            public string groupName { get; set; }
            public IpInfoDb ipInfoDb { get; set; }
            public List<SitePage> sitePage { get; set; }
        }
    }
}
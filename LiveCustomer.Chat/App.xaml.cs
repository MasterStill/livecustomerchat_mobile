//using System;
//using System.Collections.Generic;
//using System.Linq;
//using Xamarin.Forms;
//using Xamarin.Forms.Xaml;

//namespace LiveCustomer.Chat
//{
//	//[XamlCompilation (XamlCompilationOptions.Skip)]
//	public partial class App : Application
//	{
//		public static MasterDetailPage MasterDetailPage;

//		public App ()
//		{
//			InitializeComponent ();

//			MainPage = GetMainPage();

//			MainPage.SetValue (NavigationPage.BarTextColorProperty, Color.White);
//		}

//		public static Page GetMainPage()
//		{
//			//return new MainPage();
//			return new RootPage(true);
//		}
//	}
//}
using System;
using System.Collections.Generic;
using System.Linq;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;
using Prism;
using Microsoft.Practices.Unity;
using Prism.Unity;

namespace LiveCustomer.Chat
{
    public partial class App : PrismApplication
    {
        public App(IPlatformInitializer initializer = null) : base(initializer) { }
        public bool LoggedIn = false;
        protected override void OnInitialized()
        {
            InitializeComponent();
            if (!IsLoggedIn())
            {
                NavigationService.NavigateAsync("LoginPage");
                //NavigationService.NavigateAsync("NavigationPage/DashboardPage"); 
                //NavigationService.NavigateAsync("MyNavigationPage/MyTestPage");
            }
            else
            {
                NavigationService.NavigateAsync("NavigationPage/ClientChatHeadPage");
            }
        }
        protected bool IsLoggedIn()
        {
            return LoggedIn;
        }
        protected override void RegisterTypes()
        {
            Container.RegisterTypeForNavigation<NavigationPage>();
            //Container.RegisterTypeForNavigation<MyNavigationPage>();
            Container.RegisterTypeForNavigation<LoginPage, LoginPageViewModel>();
            Container.RegisterTypeForNavigation<RecentChatListPage, RecentChatViewModel>();
            Container.RegisterTypeForNavigation<ProfilePage,ProfilePageViewModel>();
            Container.RegisterTypeForNavigation<MessagesListPage>();
            Container.RegisterTypeForNavigation<ChatMessagesListPage, ChatMessageListViewModel>();
            //Container.RegisterTypeForNavigation<RecentChatItemTemplate, RecentChatItemViewModel>();
            //Container.RegisterTypeForNavigation<ChatSingle, ChatSingleViewModel>();
            //Container.RegisterTypeForNavigation<MainPage>();
            //Container.RegisterTypeForNavigation<PopupPage>();
            //Container.RegisterTypeForNavigation<MyTestPage>();
            //Container.RegisterTypeForNavigation<SupportPopUpPage>();
            //Container.RegisterTypeForNavigation<SupportPopupPage>();
            //Container.RegisterTypeForNavigation<AdminChatHeadPage>();
            //Container.RegisterTypeForNavigation<ChatHeadPage>();
            //Container.RegisterTypeForNavigation<UserDetailPage, UserDetailPageViewModel>();
        }
    }
}
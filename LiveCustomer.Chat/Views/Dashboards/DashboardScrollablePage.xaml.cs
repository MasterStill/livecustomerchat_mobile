using System;
using System.Collections.Generic;
using Xamarin.Forms;

namespace LiveCustomer.Chat
{
	public partial class DashboardScrollablePage : ContentPage
	{
		public DashboardScrollablePage()
		{
			InitializeComponent();

			BindingContext = new DashboardScrollableViewModel();
		}
	}

}

using Xamarin.Forms;

namespace LiveCustomer.Chat
{
	public partial class DashboardMultipleTilesPage : ContentPage
	{
		public DashboardMultipleTilesPage ()
		{			
			InitializeComponent();

			BindingContext = new DashboardMutipleTilesViewModel ();
		}
	}
}
using System;
using System.Collections.Generic;

using Xamarin.Forms;

namespace LiveCustomer.Chat
{
	public partial class SocialVariantPage : ContentPage
	{
		public SocialVariantPage ()
		{
			InitializeComponent ();

			BindingContext = new SocialViewModel ();
		}
	}
}
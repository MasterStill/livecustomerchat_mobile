using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Diagnostics;
using System.Linq;
using Xamarin.Forms;

namespace LiveCustomer.Chat
{
    public partial class ChatMessagesListPage : ContentPage
    {
        public ChatMessagesListPage()
        {
            InitializeComponent();
        }
        private async void OnItemTapped(object sender, ItemTappedEventArgs args)
        {
            //((ChatMessageListViewModel)this.BindingContext).ShowUserDetails.Execute((SignalRClient.EventMessage)args.Item);
            if (args == null) return;
            ((ListView)sender).SelectedItem = null;
        }
        private void OnItemAppearing(object sender, ItemVisibilityEventArgs e)
        {
            //return;
            var listView = (sender as ListView);
            if ((SignalRClient.EventMessage)e.Item == (listView.ItemsSource as ObservableCollection<SignalRClient.EventMessage>).Last())
            {
                listView.SelectedItem = e.Item;
                //listView.ScrollTo(e.Item, ScrollToPosition.MakeVisible, false);
            }
        }
        private void OnItemSelected(object sender, SelectedItemChangedEventArgs e)
        {
            try
            {                
                var listView = (sender as ListView);
                listView.ScrollTo(e.SelectedItem, ScrollToPosition.End, false);                
                //Try avoids IndexOutOfRange at first launch
            }
            catch (Exception)
            {

            }
        }
        public void OnRefreshing(object sender, EventArgs e)
        {
            var listView = (sender as ListView);
            listView.EndRefresh();            
        }

        public void animateIn(View uiElement)
        {
            animateItem(uiElement, 10500);
        }

        private void animateItem(View uiElement, uint duration)
        {
            uiElement.RotateYTo(99, duration, Easing.SinInOut);
        }

    }
}


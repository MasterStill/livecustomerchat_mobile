using System;
using System.Collections.Generic;

using Xamarin.Forms;

namespace LiveCustomer.Chat
{
    public partial class RecentChatListPage : ContentPage
    {
        public bool open;
        public RecentChatListPage()
        {
            open = false;
            InitializeComponent();
            //this.BindingContext = new RecentChatViewModel( SampleData.RecentChatMessagesList );
        }
        private async void OnItemTapped(object sender, ItemTappedEventArgs args)
        {
            var stack = Navigation.NavigationStack;
            Device.StartTimer(TimeSpan.FromSeconds(3), () =>
            {
                open = false;
                return false;
            });
            if (stack[stack.Count - 1].GetType() != typeof(ChatMessagesListPage))
            {
                var count = stack.Count;
                if (open == false)
                {
                    open = true;
                    ((RecentChatViewModel)this.BindingContext).cmdShowUserMessages.Execute((SignalRClient.CustomerSupportUser)args.Item);
                }
            }
        }
    }
}
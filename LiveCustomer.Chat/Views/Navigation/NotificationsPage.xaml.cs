using System;
using System.Collections.Generic;

using Xamarin.Forms;

namespace LiveCustomer.Chat
{
	public partial class NotificationsPage : ContentPage
	{
		public NotificationsPage()
		{
			InitializeComponent();
			BindingContext = new NotificationsViewModel();
		}
	}
}


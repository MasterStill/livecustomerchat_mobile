using System.Collections.Generic;

namespace LiveCustomer.Chat
{
	public class DashboardViewModel
	{
		public List<SampleCategory> Items { 
			get 
			{ 
				return SamplesDefinition.SamplesCategoryList;
			} 
		}
	}
}
﻿using System;
using System.Diagnostics;
using Prism.Commands;
using Prism.Mvvm;
using Prism.Navigation;
using Xamarin.Forms;
using Plugin.Connectivity;
using Xamarin.Auth;
namespace LiveCustomer.Chat
{
    public class LoginPageViewModel : BindableBase, INavigationAware
    {
        static Global g = new Global();
        public SignalRClient sSignalRClient;
        public DelegateCommand SignUpCommand { get; set; }
        public DelegateCommand LoginCommand { get; set; }
        private bool _isConnected;
        public bool IsConnected
        {
            get { return _isConnected; }
            set { SetProperty(ref _isConnected, value); }
        }
        private bool _isBusy;
        public bool IsBusy
        {
            get { return _isBusy; }
            set { SetProperty(ref _isBusy, value); }
        }
        private bool _loginEnabled;
        public bool LoginEnabled
        {
            //get { return true; }
            get { return _loginEnabled; }
            set
            {
                SetProperty(ref _loginEnabled, value);

            }
        }
        private string _networkMessageTitle;
        public string NetworkMessageTitle
        {
            get { return _networkMessageTitle; }
            set { SetProperty(ref _networkMessageTitle, value); }
        }
        private INavigationService NavigationService;
        public LoginPageViewModel(INavigationService navigationService)
        {
            if (CrossConnectivity.Current.IsConnected)
            {
                NetworkMessageTitle = "Connecting To Server...";
                // Ping and let know...
            }
            else
            {
               NetworkMessageTitle = "!Not Connected To Internet!";
            }

            LoginEnabled = false;
            this.NavigationService = navigationService;
            //SignUpCommand = new DelegateCommand(onSignup);
            LoginCommand = new DelegateCommand(onLogin);
        }
        //private async void onSignup()
        //{
        //    await _navigationService.PushAsync(Sign)
        //    //No Logic for signup now !
        //   Debug.WriteLine("Signup Button Clicked");
        //}
        private void onLogin()
        {
            IsBusy = true;
            sSignalRClient.AdminAuthorization("masterstill@gmail.com", "123!@#Subinn!!");
            //_navigationService.NavigateAsync("ChatHeadPage");            
        }

        public void OnNavigatedFrom(NavigationParameters parameters)
        {
            // throw new NotImplementedException();
        }

        public void OnNavigatedTo(NavigationParameters parameters)
        {
            sSignalRClient = new SignalRClient(g.SignalRHubFullUrl());//,navigationService);
            
            sSignalRClient.Start().ContinueWith(task =>
            {
                if (task.IsCompleted && !task.IsFaulted)
                {
                    IsConnected = true;
                    LoginEnabled = true;
                    NetworkMessageTitle = "Login";
                }
                if (task.IsFaulted)
                {
                    Debug.WriteLine("Error Occured");
                }

                //MainPage.DisplayAlert("Error", "An error occurred when trying to connect to SignalR: " + task.Exception.InnerExceptions[0].Message, "OK");
            });
            Device.StartTimer(TimeSpan.FromSeconds(10), () =>
            {
                if (!sSignalRClient.IsConnectedOrConnecting)
                {
                    IsConnected = false;
                    LoginEnabled = false;
                    sSignalRClient.Start().ContinueWith(task =>
                    {
                        if (task.IsCompleted && !task.IsFaulted)
                        {
                            IsConnected = true;
                            LoginEnabled = true;
                            NetworkMessageTitle = "Login";
                        }
                        if (task.IsFaulted)
                        {
                            Debug.WriteLine("Error Occured");
                        }

                        //MainPage.DisplayAlert("Error", "An error occurred when trying to connect to SignalR: " + task.Exception.InnerExceptions[0].Message, "OK");
                    });
                }
                return true;
            });

            if (sSignalRClient.IsConnectedOrConnecting)
            {
                sSignalRClient.onLoginAttempt += (h) =>
                {
                    if (h.Success)
                    {
                        Device.BeginInvokeOnMainThread(async () =>
                        {
                            var par = new NavigationParameters();
                            par.Add("signalrclient", sSignalRClient);
                            await NavigationService.NavigateAsync("NavigationPage/RecentChatListPage", par);
                            //await NavigationService.NavigateAsync("myapp:///NavigationPage/RecentChatListPage", par);
                        });
                    }
                    //chatReply = h.FirstOrDefault().fullName;  
                };
            }
        }

        public void OnNavigatingTo(NavigationParameters parameters)
        {
            throw new NotImplementedException();
        }
    }

}


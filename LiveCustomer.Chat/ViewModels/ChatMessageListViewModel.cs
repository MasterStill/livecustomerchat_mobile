﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Diagnostics;
using System.Linq;
using System.Windows.Input;
using Prism.Mvvm;
using Prism.Navigation;
using Xamarin.Forms;
using static LiveCustomer.Chat.SignalRClient;
using System.Threading.Tasks;

namespace LiveCustomer.Chat
{
    public class SignalRMessageToSend
    {
        public string Message { get; set; }
        public string Group { get; set; }
    }
    public class ChatMessageListViewModel : BindableBase, INavigationAware
    {

        public ChatMessageListViewModel(INavigationService navigationService)
        {
            //ChatMessagesListView = new StackLayout();
            this.NavigationService = navigationService;
        }
        //private StackLayout _chatMessageListView;
        //public StackLayout ChatMessagesListView
        //{
        //	get { return _chatMessageListView;}
        //	set { _chatMessageListView = value;}
        //}
        private ListView _messagesListView;

        public ListView MessagesListView
        {
            get { return _messagesListView; }
            set { _messagesListView = value; }
        }

        public SignalRClient signalRClient;
        public ICommand SendMessage
        {
            get { return _sendMessage ?? (_sendMessage = new Command<string>((string Message) => SendMessageClicked(Message))); }
        }
        public ICommand ShowUserInfo
        {
            get { return _ShowUserInfo ?? (_ShowUserInfo = new Command<string>((string groupName) => ShowClientInfoClicked(groupName))); }
        }

        //public ICommand ShowUserDetails
        //{
        //    get { return _showUserDetails ?? (_showUserDetails = new Command<SignalRClient.EventMessage>((SignalRClient.EventMessage Message) => cmdShowUserDetails(Message))); }
        //}
        //private ICommand _showUserDetails;


        //public StackLayout stkLayout = new StackLayout();
        private string _title;

        private SignalRClient.CustomerSupportUser _currentUser = new SignalRClient.CustomerSupportUser();
        public SignalRClient.CustomerSupportUser CurrentUser
        {
            get { return _currentUser; }
            set { SetProperty(ref _currentUser, value); }
        }
        private ObservableCollection<SignalRClient.EventMessage> _eventMessageDto;
        public ObservableCollection<SignalRClient.EventMessage> EventMessageDto
        {
            get { return _eventMessageDto; }
            set { SetProperty(ref _eventMessageDto, value); }
        }
        private string _messageBox;
        public string MessageBox
        {
            get { return _messageBox; }
            set
            {
                SetProperty(ref _messageBox, value);
                UserTyping();
            }
        }
        public string Title
        {
            get { return _title; }
            set { SetProperty(ref _title, value); }
        }
        private ICommand _sendMessage;
        private ICommand _ShowUserInfo;

        private INavigationService NavigationService;
        private string GroupName;
        private string outgoingText;
        public string OutGoingText
        {
            get { return outgoingText; }
            set { outgoingText = value; }
        }

        public async void UserTyping()
        {
            signalRClient.UserTyping(CurrentUser.groupName);
            await Task.Delay(TimeSpan.FromSeconds(3));
            UserStoppedTyping();
        }
        public void UserStoppedTyping()
        {
            signalRClient.UserStoppedTyping(CurrentUser.groupName);
        }
        public void OnNavigatedFrom(NavigationParameters parameters)
        {

        }
        public void SendMessageClicked(string Message)
        {
            //signalRClient.SendMessage(Message,CurrentUser.groupName);
            signalRClient.SendMessage(Message, GroupName);
            MessageBox = "";
            //Debug.WriteLine("sdfsf");
        }
      
        public void OnNavigatedTo(NavigationParameters parameters)
        {
            //SetupChat();
            //SignalRClient.CustomerSupportUser u = new SignalRClient.CustomerSupportUser();


            if (parameters.ContainsKey("user"))
            {

                var aa = (SignalRClient.CustomerSupportUser)parameters.Where(x => x.Key == "user").SingleOrDefault().Value;
                Title = aa.fullName;
                //u = aa;
                //CurrentUser = (SignalRClient.CustomerSupportUser)aa;
                CurrentUser = aa;
                GroupName = aa.groupName;
            }
            if (parameters.ContainsKey("messages"))
            {

                var aaa = parameters["messages"];
                try
                {
                    EventMessageDto = new ObservableCollection<SignalRClient.EventMessage>();
                    List<RecentChatViewModel.EventMessage> l = new List<RecentChatViewModel.EventMessage>();
                    l.AddRange((List<RecentChatViewModel.EventMessage>)aaa);
                    foreach (var items in l.Where(x => x.Message != null))
                    {
                        SignalRClient.EventMessage e = new SignalRClient.EventMessage();
                        e.DateTime = items.DateTime;
                        e.Message = items.Message;
                        //e.Type = items.Type;
                        e.User = items.User;
                        EventMessageDto.Add(e);
                    }
                    //Device.BeginInvokeOnMainThread(() => {
                    //    MessagesListView.ScrollTo(EventMessageDto.Last(), ScrollToPosition.End, true);
                    //});

                }
                catch (Exception ex)
                {
                    Debug.WriteLine(ex.Message);
                }
                //EventMessageDto = ObservableCollection<EventMessage>e;
            }
            if (parameters.ContainsKey("signalrclient"))
            {
                signalRClient = (SignalRClient)parameters.Where(x => x.Key == "signalrclient").SingleOrDefault().Value;
                signalRClient.onNewMessage += (h) =>
                {
                    try
                    {
                        EventMessage e = new EventMessage();
                        if (h.groupName == GroupName)
                        EventMessageDto.Add(new EventMessage
                        {
                            Message = h.Message,
                            DateTime = DateTime.Now,
                            User = new SignalRClient.CustomerSupportUserViewModel(h.User)                           
                        });                        
                        // Else below
                        // Add to propert List Now TO DO

                        //MessageDto.Message.Add(h);
                    }
                    catch (Exception ex)
                    {
                        Debug.WriteLine(ex.Message);
                    }
                };
                signalRClient.onSendPreviousMessage += (h) =>
                {
                    try
                    {                        
                        EventMessageDto = (EventMessageDto != null) ? EventMessageDto : new ObservableCollection<SignalRClient.EventMessage>();

                        List<SignalRClient.EventMessage> l = new List<SignalRClient.EventMessage>();
                        l.AddRange((List<SignalRClient.EventMessage>)h.EventMessage);
                        foreach (var items in l.Where(x => x.Message != null))
                        {
                            SignalRClient.EventMessage e = new SignalRClient.EventMessage();
                            e.DateTime = items.DateTime;
                            e.Message = items.Message;
                            e.Type = items.Type;
                            e.User = items.User;
                            EventMessageDto.Add(e);
                        }
                    }
                    catch (Exception ex)
                    {
                        Debug.WriteLine(ex.Message);
                    }
                };
                signalRClient.onGeoInformation += (h) =>
                {
                    try
                    {
                        if(h.groupName == CurrentUser.groupName)
                        {                            
                            CurrentUser.IpInfoDb = h.ipInfoDb;
                            CurrentUser.SitePages = h.sitePage;
                        }
                    }
                    catch (Exception ex)
                    {
                        Debug.WriteLine(ex.Message);
                    }
                };
            }
        }
        public void ShowClientInfoClicked(string groupName)
        {
            signalRClient.ClientGeoInformation(GroupName);
            var par = new NavigationParameters();
            par.Add("signalrclient", signalRClient);
            if(CurrentUser.IpInfoDb != null)
            {
                par.Add("geoinfoandpages", new GeoinfoAndPages {
                    groupName = CurrentUser.groupName,
                    ipInfoDb = CurrentUser.IpInfoDb,
                    sitePage = CurrentUser.SitePages                
                });
            }            
            Device.BeginInvokeOnMainThread(async () =>
            {
                try
                {
                    
                    await NavigationService.NavigateAsync("ProfilePage", par);

                }
                catch (Exception ex)
                {
                    Debug.WriteLine(ex.Message);

                }
            });
        }
        //public void cmdShowUserDetails(SignalRClient.EventMessage User)
        //{
        //    var par = new NavigationParameters();
        //    par.Add("user", User.User);
        //    Device.BeginInvokeOnMainThread(async () =>
        //    {
        //        try
        //        {
        //            await NavigationService.NavigateAsync("DashboardPage", par);

        //        }
        //        catch (Exception ex)
        //        {
        //            Debug.WriteLine(ex.Message);

        //        }

        //    });
        //}

        public void OnNavigatingTo(NavigationParameters parameters)
        {
            throw new NotImplementedException();
        }
    }
}
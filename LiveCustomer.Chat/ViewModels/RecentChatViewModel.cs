using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Diagnostics;
using System.Linq;
using System.Windows.Input;
using Prism.Mvvm;
using Prism.Navigation;
using Prism.Services;
using Xamarin.Forms;
using static LiveCustomer.Chat.SignalRClient;
using System.ComponentModel;

namespace LiveCustomer.Chat
{
    public class RecentChatViewModel : BindableBase, INavigationAware
    {
        public SignalRClient SignalRClient;
        public ICommand cmdShowUserMessages
        {
            get { return _cmdShowUserMessages ?? (_cmdShowUserMessages = new Command(() => oncmdShowUserMessagesClick())); }
        }
        public ObservableCollection<SignalRClient.CustomerSupportUser> CustomerSupportUser
        {
            get { return _customerSupportUser; }
            set { SetProperty(ref _customerSupportUser, value); }
        }
        public string Title
        {
            get { return _title; }
            set { SetProperty(ref _title, value); }
        }
        public SignalRClient.CustomerSupportUser SelectedUser
        {
            get { return _selectedUser; }
            set { SetProperty(ref _selectedUser, value); }
        }
        public ObservableCollection<GroupEventMessage> GroupMessages
        {
            get { return _groupMessages; }
            set { SetProperty(ref _groupMessages, value); }
        }
        private bool _isConnected;
        public bool IsConnected
        {
            get { return _isConnected; }
            set { SetProperty(ref _isConnected, value); }
        }
        private SignalRClient.CustomerSupportUser _selectedUser = new SignalRClient.CustomerSupportUser();
        private string _title;
        private ICommand _cmdShowUserMessages;
        private ObservableCollection<SignalRClient.CustomerSupportUser> _customerSupportUser = new ObservableCollection<SignalRClient.CustomerSupportUser>();
        private ObservableCollection<GroupEventMessage> _groupMessages = new ObservableCollection<GroupEventMessage>();
        private INavigationService NavigationService;
        private IPageDialogService _dialogService;
        private Global g = new Global();
        public class GroupEventMessage
        {
            public int Id { get; set; }
            public string GroupName { get; set; }
            public ObservableCollection<EventMessage> EventMessage { get; set; }
        }
        public class EventMessage
        {
            public DateTime DateTime { get; set; }
            public SignalRClient.enumTicketMesssages Type { get; set; }
            public SignalRClient.CustomerSupportUserViewModel User { get; set; }
            public string Message { get; set; }
        }
        public bool SupportCallingWindowCanOpen = true;
        public List<ChatMessage> Messages
        {
            get;
            set;
        }
        public RecentChatViewModel(
            //List<ChatMessage> messages,
            INavigationService navigationService
        )
        {
            this.NavigationService = navigationService;
            Title = UsersOnline();
            //Messages = messages;
        }
        private void NewSupportChatRequest(string groupName)
        {
            try
            {
                var par = new NavigationParameters();
                Device.BeginInvokeOnMainThread(async () =>
                {
                    var ConnectionUser = CustomerSupportUser.Where(x => x.groupName == groupName).SingleOrDefault();
                    List<EventMessage> E = new List<EventMessage>();
                    try
                    {
                        foreach (var items in GroupMessages.Where(x => x.GroupName == ConnectionUser.groupName).Select(x => x.EventMessage).SingleOrDefault())
                        {
                            EventMessage e = new EventMessage();
                            e.DateTime = items.DateTime;
                            e.Message = items.Message;
                            e.Type = items.Type;
                            e.User = items.User;
                            E.Add(e);
                        }
                    }
                    catch (Exception)
                    {
                        // Ask to server for previous Conversation as the group message is not there because it was the former client
                        EventMessage e = new EventMessage();
                        E.Add(e);
                    }


                    par.Add("messages", E);
                    //par.Add(groupUser);
                    //par.Add("signalrclient", SignalRClient);
                    //par.Add("user", ConnectionUser);
                    //var page = new SupportPopupPage(NavigationService, SignalRClient, ConnectionUser, E);
                    //await PopupNavigation.PushAsync(page);
                    SupportCallingWindowCanOpen = false;
                    //     var answer = await _dialogService.DisplayAlertAsync("New Chat Request", ConnectionId, "Yes", "No");
                    // if(answer == true){
                    SignalRClient.SupportRequestAccepted(groupName);
                    //NavigationService.NavigateAsync("ChatSingle", par);
                });

            }
            catch (Exception ex)
            {
                Debug.WriteLine(ex.Message);
            }
        }
        void oncmdShowUserMessagesClick()
        {
            var par = new NavigationParameters();
            par.Add("user", SelectedUser);
            List<EventMessage> E = new List<EventMessage>();
            try
            {
                foreach (var items in GroupMessages.Where(x => x.GroupName == SelectedUser.groupName).Select(x => x.EventMessage).SingleOrDefault())
                {
                    EventMessage e = new EventMessage();
                    e.DateTime = items.DateTime;
                    e.Message = items.Message;
                    e.Type = items.Type;
                    e.User = items.User;
                    E.Add(e);
                }
            }
            catch (Exception)
            {
                SignalRClient.FetchMePreviousMessage(SelectedUser.groupName);
                // Ask to server for previous Conversation as the group message is not there because it was the former client
                EventMessage e = new EventMessage();
                E.Add(e);
            }
            par.Add("messages", E);
            par.Add("signalrclient", SignalRClient);
            NavigationService.NavigateAsync("NavigationPage/ChatMessagesListPage", par, true);

            //NavigationService.NavigateAsync("NavigationPage/ChatSingle", par);
            //var item = 
            //Pagedua("More Context Action", item.CommandParameter + " more context action", "OK");
        }
        public void OnNavigatedFrom(NavigationParameters parameters)
        {


        }
        public void OnNavigatedTo(NavigationParameters parameters)
        {
            if (parameters.ContainsKey("signalrclient"))
            {
                SignalRClient = (SignalRClient)parameters.Where(x => x.Key == "signalrclient").SingleOrDefault().Value;
                SignalRClient.onJoiningAllClients += (h) =>
                {
                    foreach (var connectedClients in h)
                    {
                        try
                        {
                            CustomerSupportUser.Add(connectedClients);
                        }
                        catch (Exception ex)
                        {

                            Debug.WriteLine(ex.Message);
                        }
                        Title = UsersOnline();
                        //Only Done for users currently ( not admin )
                        //foreach (var items in h)
                        //{
                        //	GroupEventMessage g = new GroupEventMessage();
                        //	g.GroupName = items.groupName;
                        //	g.EventMessage = new ObservableCollection<EventMessage>();
                        //	GroupMessages.Add(g);
                        //}
                    }
                    //chatReply = h.FirstOrDefault().fullName;
                };
                SignalRClient.onSendPreviousMessage += (h) =>
                {
                    try
                    {
                       // GroupMessages = new ObservableCollection<GroupEventMessage>();
                        GroupEventMessage ga = (GroupMessages.Where(x => x.GroupName == h.groupName).SingleOrDefault() != null) ? GroupMessages.Where(x => x.GroupName == h.groupName).SingleOrDefault() : new GroupEventMessage();
                        ga.GroupName = h.groupName;
                        ga.EventMessage = (ga.EventMessage !=null) ? ga.EventMessage : new ObservableCollection<EventMessage>();
                        //var message = GroupMessages.Where(x => x.GroupName == messages.GroupName).SingleOrDefault();
                        //message.EventMessage = new ObservableCollection<EventMessage>();
                        foreach (var items in h.EventMessage)
                        {
                                EventMessage e = new EventMessage();
                                e.DateTime = items.DateTime;
                                e.Message = items.Message;
                                e.Type = items.Type;
                                e.User = items.User;
                                ga.EventMessage.Add(e);
                        }
                        GroupMessages.Add(ga);                        
                    }
                    catch (Exception ex)
                    {
                        Debug.WriteLine(ex.Message);
                    }
                };
                //SignalRClient.onJoiningAllSupportMessages += (h) =>
                //{
                //    GroupMessages = new ObservableCollection<GroupEventMessage>();
                //    foreach (var messages in h)
                //    {
                //        GroupEventMessage ga = new GroupEventMessage();
                //        ga.GroupName = messages.groupName;
                //        ga.EventMessage = new ObservableCollection<EventMessage>();
                //        //var message = GroupMessages.Where(x => x.GroupName == messages.GroupName).SingleOrDefault();
                //        //message.EventMessage = new ObservableCollection<EventMessage>();
                //        foreach (var items in messages.EventMessage)
                //        {
                //            EventMessage e = new EventMessage();
                //            e.DateTime = items.DateTime;
                //            e.Message = items.Message;
                //            e.Type = items.Type;
                //            e.User =items.User;
                //            ga.EventMessage.Add(e);
                //        }
                //        GroupMessages.Add(ga);
                //    }
                //};

                SignalRClient.onJoiningSingleClient += (h) =>
                {
                    try
                    {
                        if (CustomerSupportUser.Where(x => x.groupName == h.groupName).SingleOrDefault() == null)
                        {
                            CustomerSupportUser.Add(h);
                        }
                        else
                        {
                            //Already There so merge the status of user
                        }
                        Title = UsersOnline();

                    }
                    catch (Exception ex)
                    {

                        Debug.WriteLine(ex.Message);
                    }
                };
                SignalRClient.onCustomerInfoChanged += (h) =>
                {
                    try
                    {
                        var kmk = CustomerSupportUser.Where(x => x.groupName == h.groupName).SingleOrDefault();
                        CustomerSupportUser.Remove(kmk);
                        CustomerSupportUser c = new CustomerSupportUser();
                        c = kmk;
                        c.fullName = h.fullName;
                        c.avatar = h.avatar;
                        c.phoneNumber = h.phoneNumber;
                        c.email = h.email;
                        CustomerSupportUser.Add(c);


                        //workaround for timebeing !
                        return;


                        var kk = CustomerSupportUser.Where(x => x.groupName == h.groupName).SingleOrDefault();
                        kk.fullName = h.fullName;
                        kk.avatar = h.avatar;
                        kk.phoneNumber = h.phoneNumber;
                        kk.email = h.email;
                        //Title = "Change Samma Pugyo ! Customer Info)";
                    }
                    catch (Exception ex)
                    {
                        Debug.WriteLine(ex.Message);
                    }
                };
                SignalRClient.onCallingForSupport += (h) =>
                {
                    try
                    {
                        // if(SupportCallingWindowCanOpen == true)
                        {
                            NewSupportChatRequest(h);
                        }
                    }
                    catch (Exception ex)
                    {

                        Debug.WriteLine(ex.Message);
                    }
                };

                SignalRClient.onNewMessage += (h) =>
                {
                    try
                    {
                        try
                        {


                        }
                        catch { }
                        //CustomerSupportUser.FirstOrDefault().LastMessage = "Lastmessage : " + h;
                        //CustomerSupportUser.SingleOrDefault(x=>x.groupName == h)



                        //Workaround for timebeing! ***********************************************************************************************
                        var whichGroupMessage = CustomerSupportUser.SingleOrDefault(x => x.groupName == h.groupName);
                        CustomerSupportUser.Remove(whichGroupMessage);

                        CustomerSupportUser c = new CustomerSupportUser();
                        c = whichGroupMessage;
                        c.LastMessage = h.Message;
                        CustomerSupportUser.Add(c);

                        CustomerSupportUser.SingleOrDefault(x => x.groupName == h.groupName).LastMessage = h.Message;
                        var currentGroup = GroupMessages.Where(x => x.GroupName == h.groupName).SingleOrDefault();
                        if (currentGroup == null)
                        {
                            SignalRClient.FetchMePreviousMessage(h.groupName,0);
                            return;
                        }
                        EventMessage e = new EventMessage();
                        e.User = new CustomerSupportUserViewModel(h.User);
                        e.DateTime = DateTime.Now;
                        e.Message = h.Message;
                        e.Type = SignalRClient.enumTicketMesssages.TextMessage;
                        currentGroup.EventMessage.Add(e);
                        //CustomerSupportUser.Where(x => x.groupName == currentGroup.GroupName).SingleOrDefault().LastMessage = e.Message;
                    }
                    catch (Exception ex)
                    {                        
                        Debug.WriteLine("Could not find UserGroup:" + ex.Message);
                        SignalRClient.FetchMePreviousMessage(h.groupName, 0);
                    }
                };

                SignalRClient.onUserOffline += (h) =>
                {
                    try
                    {
                        //_pageDialogService.DisplayAlertAsync("New Message", h.webSite, "ok");
                        var kk = CustomerSupportUser.Where(x => x.id == h.id).SingleOrDefault();
                        CustomerSupportUser.Remove(kk);
                        Title = UsersOnline();
                    }
                    catch (Exception ex)
                    {
                        Debug.WriteLine(ex.Message);
                    }
                };
            }
            else
            {
                if (SignalRClient == null)
                    SignalRClient = new SignalRClient(g.SignalRHubFullUrl());

            }
        }
        private string UsersOnline()
        {
            return "Users Online " + CustomerSupportUser.Where(x => x.online).Count();
        }
        public void OnNavigatingTo(NavigationParameters parameters)
        {
            throw new NotImplementedException();
        }
    }
}
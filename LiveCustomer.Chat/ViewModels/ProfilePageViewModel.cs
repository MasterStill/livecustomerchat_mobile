﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Diagnostics;
using System.Linq;
using System.Windows.Input;
using Prism.Mvvm;
using Prism.Navigation;
using Xamarin.Forms;
using static LiveCustomer.Chat.SignalRClient;

namespace LiveCustomer.Chat
{
    public class ProfilePageViewModel : BindableBase, INavigationAware
    {
        private INavigationService NavigationService;
        private readonly IDeviceScreenSize deviceScreenSize;

        public ProfilePageViewModel(INavigationService navigationService)
        {            
            this.NavigationService = navigationService;
           
        }        
        public SignalRClient signalRClient;

        private string _country;
        public string Country
        {
            get { return _country; }
            set { SetProperty(ref _country, value); }
        }
        private string _image;
        public string Image
        {
            get { return _image; }
            set { SetProperty(ref _image, value); }
        }
        private SignalRClient.GeoinfoAndPages _geoInfo = new SignalRClient.GeoinfoAndPages();
        public SignalRClient.GeoinfoAndPages GeoInfo
        {
            get { return _geoInfo; }
            set { SetProperty(ref _geoInfo, value); }
        }
        
        public void OnNavigatedTo(NavigationParameters parameters)
        {            
            if (parameters.ContainsKey("geoinfoandpages"))
            {
                 var aa = (SignalRClient.GeoinfoAndPages)parameters.Where(x => x.Key == "geoinfoandpages").SingleOrDefault().Value;
                GeoInfo = aa;
                Country = GeoInfo.ipInfoDb.countryname;
                Image = "https://admin.livecustomer.chat/country/" + GeoInfo.ipInfoDb.countrycode +".png";
                ScreenSize s = (deviceScreenSize != null) ? deviceScreenSize.GetScreenSize() : new ScreenSize { X=800,Y=600};
                Image = "https://maps.googleapis.com/maps/api/staticmap?center="+ GeoInfo.ipInfoDb.cityname + "," +  GeoInfo.ipInfoDb.countrycode + "&zoom=13&scale=false&size="+ s.X +"x"+ s.Y +"&maptype=roadmap&format=png&visual_refresh=true";
            }           
            if (parameters.ContainsKey("signalrclient"))
            { 
                signalRClient = (SignalRClient)parameters.Where(x => x.Key == "signalrclient").SingleOrDefault().Value;                
                signalRClient.onGeoInformation += (h) =>
                {
                    try
                    {
                        Country = h.ipInfoDb.countryname;
                        Debug.WriteLine("asdasd");
                        //MessageDto.Message.Add(h);
                    }
                    catch (Exception ex)
                    {
                        Debug.WriteLine(ex.Message);
                    }
                };
            }
        }            
        public void OnNavigatingTo(NavigationParameters parameters)
        {
            throw new NotImplementedException();
        }

        public void OnNavigatedFrom(NavigationParameters parameters)
        {
            throw new NotImplementedException();
        }
    }
}